"""
A script for making a box-plot of the heap's runtime. Ideally,
maximum runtime will increase by a constant factor as heap size
doubles for eager operations & repair. Lazy operations are
expected to run in constant time.
"""

import csv
import matplotlib as mpl
import matplotlib.pyplot as plot

# Output our graphs to .png files.
mpl.use("agg")

# The experiments we did.
experiments = ["Eager Insertion",
               "Eager Deletion",
               "Eager Update",
               "Lazy Insertion",
               "Lazy Deletion",
               "Lazy Update",
               "Repair"]

# The number of trials we did.
m = 1000

# The dataset we'll plot.
dataset = {}

# Read raw data from the .csv we made earlier.
with open("data.csv") as data:
    reader = csv.DictReader(data)
    for row in reader:
        key = row["Size"]
        for exp in experiments:
            l = [float(row[exp + f" #{i}"]) for i in range(1, m + 1)]
            dataset[(key, exp)] = l

# Create a box plot for every experiment.
for exp in experiments:
    # Plot only the data from the current experiment.
    to_plot = [dataset[key] for key in dataset if exp in key]

    # Draw a figure.
    fig = plot.figure()
    axes = fig.add_subplot(111)

    # Set the title & axis-labels.
    axes.set_title(exp)
    axes.set_xlabel("Size of Heap (in Powers of 2)")
    axes.set_ylabel("Runtime (in Milliseconds)")

    # Feed in the data & save the figure.
    box_plot = axes.boxplot(to_plot, showfliers=False)
    fig.savefig(f"{exp} Results.png")
