from collections import deque
from bidict import bidict


class Heap():
    '''
    A class implementing a Lazy Kinetic Heap. The heap is
    a dictionary with respect to keys (which label data),
    and a heap with respect to its values (which are the
    data).
    '''

    def __init__(self):
        '''
        Initializes an empty heap with an empty
        proof. The empty heap is valid, because
        an empty proof is vacuously valid.
        '''
        self.l = [None]         # The heap itself.
        self.d = bidict()       # A 2-way dictionary for mapping keys to indices.
        self.f = set()          # The certificate failures.
        self.e = False          # Has an external event occurred?
        self.indices = deque()  # A list of indices (for repairing certificates).

    def __str__(self):
        '''
        "Pretty prints" the heap. Returns a string
        representation of the heap, where each line
        is a layer of the heap, and indices progress
        from left to right.

        For example, the heap:
             3
            / \
           1   0
        will be printed as:
        3(key1)
        1(key2)   0(key3)

        Beneath the heap, a list of certificate failures
        will also be printed.
        '''
        return f"Heap:\n{self.pp(1)}\n{self.f}"

    def pp(self, i):
        '''
        A helper method for __str__().
        '''
        if i > self.last:
            return ''
        if self.pow2(i + 1):
            return f'{self.l[i]}({self.d.inverse[i]})' + '\n' + self.pp(i + 1)
        return f'{self.l[i]}({self.d.inverse[i]})' + '   ' + self.pp(i + 1)

    def pow2(self, i):
        '''
        Returns true if i is a power of two, and false
        otherwise.
        '''
        return i and (not(i & (i - 1)))

    def __getitem__(self, key):
        '''
        Get the value associated with a particular key.
        This is mostly useful for testing; in general,
        users should already know what values correspond
        to what keys.
        '''
        return self.l[self.d[key]]

    @property
    def last(self):
        '''
        Returns the index of the last value in the list.
        '''
        return len(self.l) - 1

    def peek(self):
        '''
        Returns the maximum (i.e. root) value of the heap.
        '''
        return self.l[1]

    def addall(self, d):
        '''
        Inserts every key/value pair in dictionary d into the heap.
        '''
        for key in d:
            self.add(key, d[key])

    def ladd(self, key, value):
        '''
        Lazily inserts the value into the heap. This may
        create a certificate failure, which may invalidate
        the heap.
        '''
        self.l.append(value)
        self.d[key] = self.last
        self.register(self.last)

    def register(self, i):
        '''
        Registers the certificate failures (if any) at index
        i. This introduces a maximum of 3 failures. Also registers
        an external event, if one is caused. By any registred failure.
        '''
        # Register a certificate failure at i.
        p = self.parent(i)
        if p > 0 and self.l[i] > self.l[p]:
            self.f.add(i)

        # Register a certificate failure at i's left child.
        l = self.left(i)
        if l is not None and self.l[l] > self.l[i]:
            self.f.add(l)

        # Register a certificate failure at i's right child.
        r = self.right(i)
        if r is not None and self.l[r] > self.l[i]:
            self.f.add(r)

        # Register an external event.
        if self.l[i] > self.peek():
            self.e = True

    def add(self, key, value):
        '''
        Inserts the value into the heap, then stores the
        index of the value, so it can be referenced by its
        key.
        '''
        self.l.append(value)
        self.d[key] = self.last
        self.swim(self.last)

    def lpop(self, key):
        '''
        Lazily deletes a value from the heap. This may create a
        certificate failure, which may cause an external event.
        '''
        # We're going to move the last element, so its certificate
        # needs to be removed from the set of failures.
        if self.last in self.f:
            self.f.remove(self.last)

        i = self.d[key]    # Get the index of key's value.

        # Swap node i with the last node, then delete the last node.
        self.swap(i, self.last)
        self.d.pop(key)
        self.l.pop()

        # Register potential failures (and their corresponding events).
        if i <= self.last:
            self.register(i)

    def pop(self, key):
        '''
        Removes the given key from the heap, along with any
        data stored under it. Does not create a new certificate
        failure.
        '''
        self.lpop(key)
        self.repair_all()

    def lupdate(self, key, value):
        '''
        Lazily updates the value of a given key in the heap. This may
        create a certificate failure, which may cause an external event.
        '''
        i = self.d[key]
        self.l[i] = value

        # Register potential failures (and their corresponding events).
        self.register(i)

    def update(self, key, value):
        '''
        Updates the value of a given key in the heap, and
        maintains the heap property. Note that this is not
        allowed if certificate failures exist!
        '''
        self.pop(key)
        self.add(key, value)

    def repair_all(self):
        '''
        Repairs every certificate failure.
        '''
        for fail in self.f:
            self.repair(fail)
        self.f = set()

    def repair(self, i):
        '''
        Repairs the certificate failure at index i; that is,
        we restore the property l[i] < l[parent(i)].
        '''
        if i is None:
            return                # Disallow calls on empty subheaps.
        self.indices = deque()              # Make a list of indices we touch.
        a = self.ancestors(self.parent(i))  # Note that this excludes node i!
        d = self.descendants(i)             # This contains node i.
        m = self.merge(a, d)
        self.copybranch(m, i)

    def copybranch(self, m, i):
        '''
        Copies deque m into the branch containing i. We marked
        this branch by calling ancestors() and descendants().
        '''
        # Delete all the keys we need to overwrite.
        for tup in m:
            del(self.d[tup[1]])

        # Assign values to indices, and indices to keys.
        for i in self.indices:
            tup = m.popleft()
            self.l[i] = tup[0]
            self.d[tup[1]] = i

    def merge(self, x, y):
        '''
        Merges x and y in O(|x| + |y|) time. Assumes x and
        y are deques from the python standard library.
        '''
        d = deque()
        while(len(x) > 0 and len(y) > 0):
            if x[0] > y[0]:
                d.append(x.popleft())
            else:
                d.append(y.popleft())
        while len(x) > 0:
            d.append(x.popleft())
        while len(y) > 0:
            d.append(y.popleft())
        return d

    def ancestors(self, i):
        '''
        Makes a list of every valid ancestor of node i. This
        list will INCLUDE node i!  Stops at the root, or when
        an ancestor has a certificate failure. Also marks the
        path it takes, so we know where to copy the data later.
        '''
        # Track our path.
        self.indices.appendleft(i)

        # Base case: end of heap (p=None) or certificate failure.
        p = self.l[self.parent(i)]
        if p is None or p < self.l[i]:
            d = deque()

        # Recursive case: node i less than its (existing) parent.
        else:
            d = self.ancestors(self.parent(i))

        # Append the pair (value of node, key of node) and return.
        tup = (self.l[i], self.d.inverse[i])
        d.append(tup)
        return d

    def descendants(self, i):
        '''
        Makes a list of every valid maximum descendant of node
        i, including node i itself. Stops at a leaf, or when
        a descendant has a certificate failure. Also marks the
        path it takes in self.indices, so we know where to copy
        the data later.
        '''
        # Track our path.
        self.indices.append(i)

        # Get the INDEX of the max child of i.
        j = self.maxind(i)

        # Base case: end of heap.
        if j is None or j > self.last:
            d = deque()

        # Base case: certificate failure.
        elif self.maxchild(i) > self.l[i]:
            d = deque()

        # Recursive case: node i greater than (existing) child.
        else:
            d = self.descendants(j)

        tup = (self.l[i], self.d.inverse[i])
        d.appendleft(tup)
        return d

    def sink(self, i):
        '''
        Performs the familiar "sink" operation, which bubbles
        a value down the heap until it is greater than its child.
        '''
        # Ignore illegal values.
        if i < 1:
            return
        # Stop recursion if we hit the bottom of the heap.
        if i is None or i >= self.last:
            return
        # Stop recursion if this node is greater than its largest child.
        if self.maxchild(i) is None or self.l[i] > self.maxchild(i):
            return
        # Otherwise, keep swimming.
        j = self.maxind(i)
        self.swap(i, j)
        self.sink(j)

    def maxchild(self, i):
        '''
        Returns the VALUE of node i's largest child.
        '''
        if self.maxind(i) is None:
            return None
        return self.l[self.maxind(i)]

    def maxind(self, i):
        '''
        Returns the INDEX of node i's largest child.
        '''
        if self.left(i) is None:
            return self.right(i)
        if self.right(i) is None:
            return self.left(i)
        if self.l[self.left(i)] > self.l[self.right(i)]:
            return self.left(i)
        return self.right(i)

    def swim(self, i):
        '''
        Performs the familiar "swim" operation, which bubbles
        a value up the heap until it is less than its parent.
        '''
        if i <= 1 or self.l[i] < self.l[self.parent(i)]:
            return
        self.swap(i, self.parent(i))
        self.swim(self.parent(i))

    def swap(self, i, j):
        '''
        Swaps the list items at indices i and j. Also manages
        the dictionary entries that refer to i and j, so we can
        update values correctly.
        '''
        # Swap the dictionary entries that refer to i and j.
        ikey = self.d.inverse[i]
        jkey = self.d.inverse[j]
        self.d[ikey] = None     # Avoids duplicate values in bidict.
        self.d[jkey] = i
        self.d[ikey] = j

        # Swap the list items at indices i and j.
        tval = self.l[i]
        self.l[i] = self.l[j]
        self.l[j] = tval

    def parent(self, i):
        '''
        Returns the index of node i's parent.
        '''
        return i // 2

    def right(self, i):
        '''
        Returns the index of node i's right child, or None
        if node i has no right child.
        '''
        c = 2 * i + 1
        if c > self.last:
            return None
        return c

    def left(self, i):
        '''
        Returns the index of node i's left child, or None
        if node i has no left child.
        '''
        c = 2 * i
        if c > self.last:
            return None
        return c

    def isheap(self, i):
        '''
        Returns true if i is greater than all its descendants (or
        if i is NoneType, and therefore has no children); returns
        false otherwise. isheap(1) verifies the entire heap.
        '''
        if i is None:
            return True
        if self._greatest(i):
            return self.isheap(self.left(i)) and self.isheap(self.right(i))
        return False

    def _greatest(self, i):
        '''
        Returns true if i is greater than its children, and false
        otherwise. This is a helper method for isheap(), so you
        probably shouldn't be calling greatest()!
        '''
        if self.left(i) is not None and self.l[self.left(i)] > self.l[i]:
            return False
        if self.right(i) is not None and self.l[self.right(i)] > self.l[i]:
            return False
        return True
