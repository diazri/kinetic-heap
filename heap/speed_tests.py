"""
This is a script for speed testing eager changes to the heap.
"""
import csv
import random
import time
from statistics import mean

from heap import Heap


results = {}


def build_heap(n):
    """
    Return a randomly-generated heap of size n.
    """
    h = Heap()
    for i in range(n):
        h.add(f"key{i}", random.randint(0, 10 * n))
    return h


def time_eager_insert(h, n):
    """
    Eagerly insert an item into a heap of size n, and return the
    run-time of the insertion algorithm (in milliseconds).
    """
    key = f"key{n}"
    val = random.randint(0, 10 * n)

    start = time.time() * 1000
    h.add(key, val)
    end = time.time() * 1000

    h.pop(key)         # Important that we clean up after ourselves.
    return end - start


def time_eager_delete(h, n):
    """
    Eagerly delete an item from a heap of size n, and return the
    run-time of the deletion algorithm (in milliseconds).
    """
    i = random.randint(0, n - 1)
    key = f"key{i}"
    val = h[key]

    start = time.time() * 1000
    h.pop(key)
    end = time.time() * 1000

    h.add(key, val)         # Important that we clean up after ourselves.
    return end - start


def time_eager_update(h, n):
    """
    Eagerly delete an item from a heap of size n, and return the
    run-time of the deletion algorithm (in milliseconds).
    """
    i = random.randint(0, n - 1)
    key = f"key{i}"
    val = random.randint(0, 10 * n)

    start = time.time() * 1000
    h.update(key, val)
    end = time.time() * 1000

    return end - start


def time_lazy_insert(h, n):
    """
    Lazily insert one item into a heap of size n, and return the
    run-time of the insertion algorithm.
    """
    key = f"key{n}"
    val = random.randint(0, 10 * n)

    start = time.time() * 1000
    h.ladd(key, val)
    end = time.time() * 1000

    return end - start


def time_lazy_delete(h, n):
    """
    Lazily insert one item into a heap of size n, and return the
    run-time of the insertion algorithm.
    """
    i = random.randint(0, n - 1)
    key = f"key{i}"

    start = time.time() * 1000
    h.lpop(key)
    end = time.time() * 1000

    return end - start


def time_lazy_update(h, n):
    """
    Lazily insert one item into a heap of size n, and return the
    run-time of the insertion algorithm.
    """
    i = random.randint(0, n - 1)
    key = f"key{i}"
    val = random.randint(0, 10 * n)

    start = time.time() * 1000
    h.lupdate(key, val)
    end = time.time() * 1000

    return end - start


def time_repair(h):
    """
    Repair the O(1) certificate failures produced while testing
    the lazy methods, and return the run-time of the repair alg.
    """
    start = time.time() * 1000
    h.repair_all()
    end = time.time() * 1000

    return end - start


def test_each(h, n, i):
    """
    Test the run-time of each method exactly once, and save the
    results to the global variable "results."
    """
    # Speed of eager operations.
    results[f"Eager Insertion #{i}"] = time_eager_insert(h, n)
    results[f"Eager Deletion #{i}"] = time_eager_delete(h, n)
    results[f"Eager Update #{i}"] = time_eager_update(h, n)

    # Speed of lazy operations.
    results[f"Lazy Insertion #{i}"] = time_lazy_insert(h, n)
    results[f"Lazy Update #{i}"] = time_lazy_update(h, n)
    results[f"Lazy Deletion #{i}"] = time_lazy_delete(h, n)

    # Speed to repair the O(1) failures created by lazy operations.
    results[f"Repair #{i}"] = time_repair(h)


def run_all(n, m):
    """
    Run speed tests on a heap of size n, and save the results to
    the global variable "results".
    """
    results.clear()
    results["Size"] = n

    for i in range(1, m + 1):
        h = build_heap(n)
        test_each(h, n, i)

    l = [results[key] for key in results if "Eager I" in key]
    avg = mean(l)
    results["Average Eager Insertion"] = avg
    print(f"Average eager insertion: {avg}")

    l = [results[key] for key in results if "Eager D" in key]
    avg = mean(l)
    results["Average Eager Deletion"] = avg
    print(f"Average eager deletion: {avg}")

    l = [results[key] for key in results if "Eager U" in key]
    avg = mean(l)
    results["Average Eager Update"] = avg
    print(f"Average eager update: {avg}")

    l = [results[key] for key in results if "Lazy I" in key]
    avg = mean(l)
    results["Average Lazy Insertion"] = avg
    print(f"Average lazy insertion: {avg}")

    l = [results[key] for key in results if "Lazy D" in key]
    avg = mean(l)
    results["Average Lazy Deletion"] = avg
    print(f"Average lazy deletion: {avg}")

    l = [results[key] for key in results if "Lazy U" in key]
    avg = mean(l)
    results["Average Lazy Update"] = avg
    print(f"Average lazy update: {avg}")

    l = [results[key] for key in results if "Repair" in key]
    avg = mean(l)
    results["Average Lazy Update"] = avg
    print(f"Average repair: {avg}")


def main():
    """
    Test heaps from size 1 to n, and write the resulting data to a .csv.
    """
    n = 1000000
    m = 1000
    with open("data.csv", "w") as data:
        run_all(2, m)
        data_writer = csv.DictWriter(data, fieldnames=results.keys())
        data_writer.writeheader()
        i = 1
        while i <= n:
            print(f"Testing heap of size {i}.")
            run_all(i, m)
            data_writer.writerow(results)
            i *= 2


if __name__ == "__main__":
    main()
