"""
A module for unit testing my heap class. Each class
in the module tests a different set of functions.
"""
import unittest
import random
from string import ascii_lowercase

from collections import deque
from heap import Heap




class ControlledTest(unittest.TestCase):
    """
    A class to test heap functions in a controlled environment.
    """

    def setUp(self):
        """
        Populates a test heap with nonrandom data.
        """
        self.h = Heap()
        self.d = {}
        i = 0
        for key in ascii_lowercase:
            random.seed(i)
            value = random.randint(0, 100)
            self.d[key] = value
            self.h.add(key, value)
            i += 1
        # Some helper datasets.
        self.to_ins = dict()
        self.to_del = {}
        self.to_up = dict()

    def assert_valid_heap(self):
        """
        Verify that the heap is valid; i.e. that each node
        is larger than both its children.
        """
        self.assertTrue(self.h.isheap(1))

    def assert_valid_keys(self):
        """
        Verify that key-value pairs are preserved.
        """
        for key in self.d:
            # If we updated the key, it should have a new value.
            if key in self.to_up:
                self.assertEqual(self.to_up[key], self.h[key])
            # If not, the key should,
            else:
                # Match its original value.
                try:
                    self.assertEqual(self.d[key], self.h[key])
                # Or be missing, because we deleted it.
                except KeyError:
                    self.assertIn(key, self.to_del)

    def assert_invalid_heap(self):
        """
        Verify that the heap is NOT valid; i.e. that some
        node is larger than its parent.
        """
        self.assertFalse(self.h.isheap(1))

    def test_insertion(self):
        """
        Make sure insertion works.
        """
        self.assert_valid_heap()
        self.assert_valid_keys()

    def test_lazy_insertion(self):
        """
        Lazy inserts 3 elements, then restores the heap property.
        """
        # Perform the insertions, and make sure the heap is NOT valid!
        self.to_ins = {'1': 100, '2': -100, '3': 5}
        for key in self.to_ins:
            self.h.ladd(key, self.to_ins[key])

        self.assert_invalid_heap()
        self.assert_valid_keys()

        # Repair the heap.
        self.h.repair_all()
        self.assert_valid_heap()
        self.assert_valid_keys()

    def test_deletion(self):
        """
        Delete an item, then check if the heap is still valid.
        """
        # Do the deletion, and make sure the heap stays valid.
        self.to_del = {"a", "b", "c"}
        for key in self.to_del:
            self.h.pop(key)
        # Verify that the heap is valid.
        self.assert_valid_heap()
        self.assert_valid_keys()

    def test_lazy_deletion(self):
        """
        Lazy deletes 3 elements, then restores the heap property.
        """
        # Delete 3 elements, and make sure the heap is NOT valid!
        self.to_del = {"a", "f", "z"}
        for key in self.to_del:
            self.h.lpop(key)
        self.assert_invalid_heap()

        # Restore the heap property.
        self.h.repair_all()
        self.assert_valid_heap()
        self.assert_valid_keys()

    def test_update(self):
        """
        Update an item, then check if the heap is still valid.
        """
        print(f"Before eager update: {self.h}")
        self.to_up = {'a': 100, 'f': -100, 'z': 5}
        for key in self.to_up:
            self.h.update(key, self.to_up[key])
            print(f"After updating key {key}: {self.h}")
        self.assert_valid_heap()
        self.assert_valid_keys()

    def test_lazy_update(self):
        """
        Lazy updates 3 elements, then restores the heap property.
        """
        # Update the elements.
        to_up = {'a': 100, 'f': -100, 'z': 5}
        for key in to_up:
            self.h.lupdate(key, to_up[key])
        self.assertFalse(self.h.isheap(1))

        print(f"Heap after updates:\n{self.h}")

        # Repair the heap.
        self.h.repair_all()
        self.assertTrue(self.h.isheap(1))


class TestHelpers(unittest.TestCase):
    """
    A class to test helper functions that the heap depends on.
    Conducts tests in a controlled environment with nice properties.
    """

    def setUp(self):
        """
        Populates a test heap with nonrandom data.
        """
        self.h = Heap()
        i = 99
        for c in ascii_lowercase:
            self.h.add(c, i)
            i = i - 1
        self.h.indices = deque()
        self.h.keys = deque()
        self.expected = {(99, "a"),
                         (98, "b"),
                         (96, "d"),
                         (92, "h"),
                         (84, "p")}

    def test_descendants(self):
        """
        Tests the descendants() method.
        """
        des = self.h.descendants(1)

        # Make sure the descendant values are correct.
        self.assertEqual(len(self.expected), len(des))
        prev = 100
        for tup in des:
            self.assertIn(tup, self.expected)
            self.assertGreater(prev, tup[0])
            prev = tup[0]

        # Make sure the index values are correct.
        self.assertEqual(len(self.expected), len(self.h.indices))
        cmp = 1
        for i in self.h.indices:
            self.assertEqual(i, cmp)
            cmp *= 2

    def test_ancestors(self):
        """
        Tests the ancestors() method.
        """
        anc = self.h.ancestors(16)
        self.assertEqual(len(self.expected), len(anc))
        prev = 100

        # Make sure the ancestor values are correct.
        for tup in anc:
            self.assertIn(tup, self.expected)
            self.assertGreater(prev, tup[0])
            prev = tup[0]

        # Make sure the index values are correct.
        self.assertEqual(len(self.expected), len(self.h.indices))
        cmp = 1
        for i in self.h.indices:
            self.assertEqual(i, cmp)
            cmp *= 2

    def test_merge(self):
        """
        Tests the merge() method.
        """
        a = deque([8, 7, 6, 4, 1])
        b = deque([9, 5, 3, 2, 0])
        m = self.h.merge(a, b)
        for i in range(0, 10):
            self.assertEqual(9 - i, m[i])


if __name__ == '__main__':
    unittest.main()
