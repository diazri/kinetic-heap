\documentclass{article}

\usepackage{dirtytalk}

\author{Reuben Diaz}
\title{Manual Pages for the Lazy Kinetic Heap}

\begin{document}

\maketitle

\begin{abstract}
	This document is a user manual for the Lazy
        Kinetic Heap class. It provides an introduction
        to the Kinetic Data Structures framework, a layout
        of the Heap's API, and a sample use case
        where the Lazy Kinetic Heap is preferable to both
        a static heap and a conventional Kinetic Heap.
\end{abstract}

\section{Introduction}

\subsection{What is a Kinetic Data Structure?}

A Kinetic Data Structure (KDS) is a data structure designed
to maintain a property on a changing set of data. A Kinetic
Heap, for example, maintains the max on a set of one-dimensional
data points. More complex KDSs, such as red-black trees,
convex hulls, and closest pairs, can maintain more interesting
properties --- a complete sort order, the convex hull on a set
of points, or the closest pair in a set of points.

To do this, the KDS maintains a set of \emph{certificates},
which correspond to the property we want to maintain. In a
Kinetic Heap, for example, each node has a certificate to
verify that it is less than its parent. As the data changes,
certificates may \emph{fail} --- for example, a node may
become greater than its parent. When a certificate fails,
the KDS must \emph{repair} itself, to re-establish the desired
property. In the case of a Kinetic Heap, this is accomplished
by swapping the failed node with its parent, and creating
a new certificate that verifies it is larger.

\subsection{Why Use a Kinetic Data Structure?}\label{sec:whykds}

In real-time applications, and in computer simulations with
lots of movement, it is very inefficient to recompute a property
over and over again, especially on a large dataset. For example,
to maintain the max on a set of $n$ constantly-changing values,
the best naive approach would be to iterate over all $n$ values
whenever any one of them changes. This is not practical for large
datasets, or datasets that change often.

A Kinetic Heap makes the above computation much more efficient.
After constructing the heap in $O(n \log n)$ time, a changed value
can be handled in $O(1)$ time! This enables us to maintain the max
of a very large set of values that change very quickly. However,
this speed depends on four vital assumptions:

\begin{enumerate}
	\item We know each data-point's movement path in advance.

	\item The data moves continuously.

	\item We have a \emph{total ordering} of the certificate
		failures. In other words, at any point in time,
		we know exactly which certificate will fail next.

	\item No two certificates fail at exactly the same time.
\end{enumerate}

The first assumption is required in order to obtain a total ordering
of certificate failures. Certificates' failure times are calculated
based on the movement paths of the data points they consider, so if
even one movement path is missing, it is impossible to calculate a
total ordering.

The second assumption is required for most repair algorithms. The
Kinetic Heap, for example, swaps the two data-points that cause a
certificate failure. If movement is discontinuous, then that swap
may not be enough to repair the Heap (i.e. the child node may be
greater than the root, instead of just its parent).

The third assumption is required to run the repair algorithm.
Traditional repair algorithms assume that the rest of the KDS is
valid, so processing certificates in order is the only way for
these algorithms to guarantee correctness.

The fourth assumption is required for a similar reason. If two
certificates fail at exactly the same time, then there is no
way to order their failures, so the repair algorithm cannot
guarantee correctness.

These assumptions make KDSs impractical for many real-time applications.
Many interesting datasets --- including computer networks, vehicle
traffic, and financial markets --- move in ways we cannot predict,
and move discontinuously. Furthermore, obtaining a total ordering of
certificate failures is computationally expensive, and it is difficult
to guarantee that certificates will never fail at the same time.

\subsection{What is a Lazy Kinetic Data Structure?}

A Lazy Kinetic Data Structure, or Lazy KDS, is a KDS that relaxes
the problematic assumptions above. Instead of anticipating certificate
failures and fixing them one-at-a-time, the Lazy KDS lets an arbitrary
number of certificates fail, and fixes the resulting failures only
when necessary.

For example, instead of swapping values immediately after each failure,
a Lazy Kinetic Heap performs a mergesort-like operation along the
branch containing each certificate failure. This makes the repair
algorithm must more robust, enabling it to handle both extreme changes
in value and cases when multiple failures exist at once.

A more in-depth discussion of the repair algorithm will be available in
a forthcoming theoretical paper on the Lazy Kinetic Heap.

\subsection{Why Use a Lazy Kinetic Data Structure?}\label{sec:why}

Lazy Kinetic Data Structures alleviate the problems outlined at the
end of section \ref{sec:whykds}. Lazy KDSs are especially useful for
real-time computing, because it is often difficult (or impossible) to
accurately estimate the movement paths of real-world phenomena.

Use cases like this appear often in finance, especially in algorithmic
trading. For example, we could use a Lazy Kinetic Heap to perform
\emph{arbitrage} --- buying something on one market, while simultaneously
selling it at a higher price on another market. To do this effectively, we
need our program to take in real-time information from several markets,
and quickly find the best arbitrage opportunity. We say an opportunity
is the best if it has the maximum difference between purchase and sale
price. In other words, the best opportunity makes us the most money.

To show how the Lazy Kinetic Heap solves the above problem, consider a
single commodity, $c$. We can model each opportunity to arbitrage $c$
as a node in a Heap. We label the node using two of the markets
($a$ and $b$) on which it is sold. We assign the node a value equal to the
difference between its price on market $b$ and its price on market $a$.
And, now that our Heap has been built, the node on top will tell us which
two markets, $a'$ and $b'$, give us the best return on investment. We can
simultaneously buy $c$ on market $a'$ and sell $c$ on market $b'$ for
maximum profit.

But, you might wonder, where does the \emph{kinetic} element come in?
The answer is simple. Since the time we started building the heap, the
market has already changed, and sent us a number of updates. If
we're using a static heap, we may miss a better opportunity. Even worse,
the opportunity we originally identified may be gone. Clearly, a static heap
is suboptimal.

Likewise, you may ask, why can't we use an ordinary Kinetic Heap? The
answer here lies in how stock prices fluctuate. Because the movement
is discontinuous and unpredictable, the classical kinetic algorithms
will not accurately maintain a heap as updates arrive.

With a Lazy Kinetic Heap, we can process every incoming update, repair
the Heap, and repeat that process until no new updates arrive. We can
then place our buy and sell orders, confident we're getting the best
bang for our buck.

Many other use cases follow similar patterns. In computer networking,
routing times from one node to another change unpredictably, so algorithms
to maintain a Lazy Kinetic Shortest Path or a Lazy Kinetic Minimum Spanning
Tree are desirable. Traffic control, missile defense systems, and many other
real-world applications benefit from similar algorithms.

\section{The Kinetic Heap API}

The Lazy Kinetic Heap is implemented in the file \say{heap.py.} A majority
of the methods aren't meant to be user-facing, so we outline the user-facing
methods below:

\subsection{Heap Operations}

\begin{itemize}
	\item peek() --- returns the maximum value in the heap.
	
	\item isHeap($i$) --- returns true if the heap rooted at node $i$ is valid
	        (i.e. if $i$ is greater than all its descendants), and false otherwise.
	        Running isHeap(1) verifies the correctness of the entire heap.
	        
	\item add(key, value) --- inserts a key, value pair into the heap.
\end{itemize}

\subsection{Eager KDS Operations}

\begin{itemize}
	\item add(key, value) --- inserts a key, value pair into the heap. This is
			 an \emph{eager insertion}, so if the heap was valid before the call
			 to add(), the heap will remain valid.
			 
	\item pop(key) --- deletes the key (and its corresponding value) from the
	        heap. This is an \emph{eager deletion}, so if the heap was valid before the call
	        to pop(), the heap will remain valid.
	        
	\item update(key, value) --- updates the value of the given key. This is an
	         \emph{eager insertion}, so if the heap was valid before the call
			to pop(), the heap will remain valid.
\end{itemize}

\subsection{Lazy KDS Operations}

\begin{itemize}
	\item ladd(key, value) --- inserts a key, value pair into the heap. This is
	a \emph{lazy insertion}, so the heap will register a certificate failure,
	and may no longer be valid.
	
	\item lpop(key) --- deletes the key (and its corresponding value) from the
	heap. This is a \emph{lazy deletion}, so the heap will register up to 2 certificate
	failures, and may no longer be valid.
	
	\item lupdate(key, value) --- updates the value of the given key. This is a
	\emph{lazy insertion}, so the heap will register up to 2 certificate failures,
	and may no longer be valid.
	
	\item repair\_all() --- repairs every certificate failure in the heap. After this
	method is run, the heap will be valid (i.e. have the correct max) again.
\end{itemize}

\subsection{A Note on Run-Time}

Let $n$ be the number of nodes in the heap, and $c$ be the number of
certificate failures in the heap. Eager operations run in $O(\log n)$
time, and Lazy operations run in $O(1)$ time, except for repair\_all(),
which runs in $O(c \log n)$ time.

\end{document}

