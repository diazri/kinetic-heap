# Lazy Kinetic Heap

## About This Project

This is a simple example of a Lazy Kinetic Data Structure --- a Kinetic
Data Structure that can handle updates in real-time, on data with
discontinuous and unpredictable fluctuations. I am currently researching
Lazy algorithms for more complex data structures, including red-black
trees and minimum spanning trees. After lazifying several data structures,
I plan to publish a paper outlining the general method for doing so.

For more information about the Lazy Kinetic Heap itself, please see the
LaTeX document in the "documentation" folder. It explains why this research
is useful, and outlines sample use-cases for the Kinetic Heap specifically.

## License

This code is available under the GNU Public License (GPL). Feel free to use
it, including for commercial purposes, without contacting or compensating
me. However, please include my name somewhere in your project's
documentation  if possible, so that people are aware my research is being
put to good use.
